﻿using UnityEngine;

namespace Karen90MmoFramework.Hud
{
	public partial class HUD
	{
		#region Constants and Fields

		private Rect rectMinimap;
		private Rect rectMinimapContainer;
		private Rect rectPlayerIcon;
		private Rect rectZoomIn;
		private Rect rectZoomOut;

		private RenderTexture textureMinimap;
		private Texture2D textureMinimapContainer;
		private Texture2D texturePlayerIcon;

		private Material materialMinimap;

		private Vector2 minimapRotatePivot;

		#endregion

		#region Public Methods

		

		#endregion

		#region Local Methods

		private void InitializeMinimapMembers()
		{
			this.textureMinimapContainer = Resources.Load("gui/frm_minimap") as Texture2D;
			this.textureMinimap = Resources.Load("Misc/MiniMapTexture") as RenderTexture;
			this.texturePlayerIcon = Resources.Load("Icons/icon_blip_player") as Texture2D;

			this.materialMinimap = Resources.Load("Misc/MinimapMask") as Material;

			this.rectMinimapContainer = new Rect()
			{
				x = Screen.width - textureMinimapContainer.width - 5,
				y = 5,
				width = textureMinimapContainer.width,
				height = textureMinimapContainer.height
			};

			this.rectMinimap = new Rect()
			{
				x = rectMinimapContainer.x - 4,
				y = rectMinimapContainer.y - 5,
				width = 190,
				height = 190
			};

			this.rectPlayerIcon = new Rect()
			{
				x = rectMinimap.x + (rectMinimap.width - texturePlayerIcon.width) / 2,
				y = rectMinimap.y + (rectMinimap.height - texturePlayerIcon.height) / 2,
				width = texturePlayerIcon.width,
				height = texturePlayerIcon.height
			};

			this.rectZoomIn = new Rect()
			{
				x = rectMinimapContainer.x + 150,
				y = rectMinimapContainer.y + 125,
				width = 23,
				height = 23
			};

			this.rectZoomOut = new Rect()
			{
				x = rectMinimapContainer.x + 134,
				y = rectMinimapContainer.y + 144,
				width = 23,
				height = 23
			};

			this.minimapRotatePivot = new Vector2()
			{
				x = rectPlayerIcon.x + texturePlayerIcon.width / 2,
				y = rectPlayerIcon.y + texturePlayerIcon.height / 2
			};
		}

		private void HandleMinimap()
		{
			Graphics.DrawTexture(rectMinimap, textureMinimap, materialMinimap);

			GUIUtility.RotateAroundPivot(player.Rotation.y, minimapRotatePivot);
			GUI.DrawTexture(rectPlayerIcon, texturePlayerIcon);
			
			GUI.matrix = this.originalGUIMatrix;

			GUI.DrawTexture(rectMinimapContainer, textureMinimapContainer);
			if (GUIX.Button(rectZoomIn, "+", guiSettings.GUIStyleButtonMinimapOption))
			{
				player.World.Map.ZoomIn();
			}

			if (GUIX.Button(rectZoomOut, "-", guiSettings.GUIStyleButtonMinimapOption))
			{
				player.World.Map.ZoomOut();
			}
		}

		#endregion
	}
}
