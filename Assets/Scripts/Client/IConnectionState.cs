﻿using ExitGames.Client.Photon;
using Karen90MmoFramework.Rpc;
using System.Collections.Generic;

namespace Karen90MmoFramework.Client
{
	public interface IConnectionState
	{
		/// <summary>
		/// Gets the connection status
		/// </summary>
		ConnectionStatus Status { get; }

		/// <summary>
		/// Handles a(n) <see cref="OperationResponse"/>.
		/// </summary>
		bool HandleOperationResponse(OperationResponse operationResponse);

		/// <summary>
		/// Handles a(n) <see cref="EventData"/>.
		/// </summary>
		bool HandleEvent(EventData eventData);

		/// <summary>
		/// Sends an operation
		/// </summary>
		void SendOperation(PhotonPeer peer, ClientOperationCode operationCode, Dictionary<byte, object> parameters, bool encrypt);
	}
}
