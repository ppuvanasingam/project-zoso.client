﻿using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public abstract class CharacterStatCalculator : IStatCalculator<short>
	{
		#region Implementation of IStatCalculator<out short>

		/// <summary>
		/// Calculates the resultant value of a <see cref="Stats"/>.
		/// </summary>
		public abstract short CalculateValue(Player player, Stats stat);

		#endregion
	}
}
