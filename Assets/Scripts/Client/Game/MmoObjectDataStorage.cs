﻿using System.Collections;
using System.Collections.Generic;

using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game
{
	public class MmoObjectDataStorage
	{
		#region Constants and Fields

		private static MmoObjectDataStorage _instance;

		/// <summary>
		/// stored properties
		/// </summary>
		private readonly Dictionary<short, Hashtable> storedProperties;

		/// <summary>
		/// cached properties
		/// </summary>
		private readonly Dictionary<MmoGuid, Hashtable> cachedProperties;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the singleton instance
		/// </summary>
		public static MmoObjectDataStorage Instance
		{
			get
			{
				return _instance ?? (_instance = new MmoObjectDataStorage());
			}
		}

		#endregion

		#region Constructors and Destructors

		private MmoObjectDataStorage()
		{
			this.storedProperties = new Dictionary<short, Hashtable>();
			this.cachedProperties = new Dictionary<MmoGuid, Hashtable>();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Loads all the stored properties
		/// </summary>
		public void Load()
		{
			var properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Kyle"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Good},
					{(byte) PropertyCode.Species, (byte) Species.Humanoid},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Civilian},
					{(byte) PropertyCode.ConversationId, (short) 1},
				};

			this.AddStoredProperties(1, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Stan"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Good},
					{(byte) PropertyCode.Species, (byte) Species.Humanoid},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Civilian},
					{(byte) PropertyCode.ConversationId, (short) 2},
				};

			this.AddStoredProperties(2, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Garrison"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Good},
					{(byte) PropertyCode.Species, (byte) Species.Humanoid},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Merchant},
				};

			this.AddStoredProperties(3, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Hungry Wolf"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Evil},
					{(byte) PropertyCode.Species, (byte) Species.Creature},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Enemy},
				};

			this.AddStoredProperties(4, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Kenny"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Good},
					{(byte) PropertyCode.Species, (byte) Species.Humanoid},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Civilian},
					{(byte) PropertyCode.ConversationId, (short) 3},
				};

			this.AddStoredProperties(5, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Hungry Monster"},
					{(byte) PropertyCode.Alignment, (byte) SocialAlignment.Evil},
					{(byte) PropertyCode.Species, (byte) Species.Creature},
					{(byte) PropertyCode.NpcType, (byte) NpcType.Enemy},
				};

			this.AddStoredProperties(6, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Forgotten Chest"},
					{(byte) PropertyCode.GoType, (byte) GameObjectType.Chest},
				};

			this.AddStoredProperties(10, properties);

			properties = new Hashtable
				{
					{(byte) PropertyCode.Name, "Kings' Foil"},
					{(byte) PropertyCode.GoType, (byte) GameObjectType.Plant},
				};

			this.AddStoredProperties(11, properties);
		}

		/// <summary>
		/// Unloads all the properties
		/// </summary>
		public void Unload()
		{
			this.ClearCachedProperties();
			this.ClearStoredProperties();
		}

		/// <summary>
		/// Adds a stored properties to storage
		/// </summary>
		void AddStoredProperties(short familyId, Hashtable properties)
		{
			this.storedProperties.Add(familyId, properties);
		}

		/// <summary>
		/// Adds a cached properties to storage
		/// </summary>
		public void AddCachedProperties(MmoGuid guid, Hashtable properties)
		{
			this.cachedProperties.Add(guid, properties);
		}

		/// <summary>
		/// Returns whether a stored properties exists in storage or not
		/// </summary>
		public bool ContainsStoredProperties(short familyId)
		{
			return this.storedProperties.ContainsKey(familyId);
		}

		/// <summary>
		/// Returns whether a cached properties exists in storage or not
		/// </summary>
		public bool ContainsCachedProperties(MmoGuid guid)
		{
			return this.cachedProperties.ContainsKey(guid);
		}

		/// <summary>
		/// Tries to retrieve a stored properties from storage
		/// </summary>
		public bool TryGetStoredProperties(short familyId, out Hashtable properties)
		{
			return this.storedProperties.TryGetValue(familyId, out properties);
		}

		/// <summary>
		/// Tries to retrieve a cached properties from storage
		/// </summary>
		public bool TryGetCachedProperties(MmoGuid guid, out Hashtable properties)
		{
			return this.cachedProperties.TryGetValue(guid, out properties);
		}

		/// <summary>
		/// Removes a cached properties from storage
		/// </summary>
		public bool RemoveCachedProperties(MmoGuid guid)
		{
			return this.cachedProperties.Remove(guid);
		}

		/// <summary>
		/// Clear all stored properties from storage
		/// </summary>
		void ClearStoredProperties()
		{
			foreach (var item in this.storedProperties)
			{
				item.Value.Clear();
			}
			this.storedProperties.Clear();
		}

		/// <summary>
		/// Clear all cached properties from storage
		/// </summary>
		public void ClearCachedProperties()
		{
			foreach (var item in this.cachedProperties)
			{
				item.Value.Clear();
			}
			this.cachedProperties.Clear();
		}

		#endregion
	}
}
