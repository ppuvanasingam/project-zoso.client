﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;

using Karen90MmoFramework.Rpc;

namespace Karen90MmoFramework.Client
{
	public class NetworkEngine : IPhotonPeerListener, IConnection, IMessageDispatcher
	{
		#region Constants and Fields

		private static readonly NetworkEngine _instance = new NetworkEngine();
		private readonly PhotonPeer peer;

		private readonly Dictionary<byte, Action<OperationResponse>> operationResponseHandlers;
		private readonly Dictionary<byte, Action<EventData>> eventHandlers;

		private readonly Dictionary<ConnectionStatus, List<EventData>> enqueuedEvents;

		private IConnectResponseHandler connectResponseHandler;
		private int outgoingMessageCount;

		#endregion

		#region Properties

		/// <summary>
		/// Instance of the NetworkEngine
		/// </summary>
		public static NetworkEngine Instance
		{
			get
			{
				return _instance;
			}
		}

		private IConnectionState connectionState;
		/// <summary>
		/// Current state of the game
		/// </summary>
		public IConnectionState GameState
		{
			get
			{
				return this.connectionState;
			}

			set
			{
				this.connectionState = value;

				List<EventData> eventList;
				if(enqueuedEvents.TryGetValue(connectionState.Status, out eventList))
				{
					var photonPeerListener = (IPhotonPeerListener) this;
					foreach (var eventData in eventList)
						photonPeerListener.OnEvent(eventData);
					enqueuedEvents.Remove(connectionState.Status);
				}
			}
		}

		/// <summary>
		/// Gets the server time stamp in milliseconds
		/// </summary>
		public int ServerTimeStamp
		{
			get { return this.peer.ServerTimeInMilliSeconds; }
		}

		#endregion

		#region Constructors and Destructors

		private NetworkEngine()
		{
			TypeSerializer.RegisterTypes();

			this.peer = new PhotonPeer(this, ConnectionProtocol.Udp) { ChannelCount = 2 };

			this.operationResponseHandlers = new Dictionary<byte, Action<OperationResponse>>();
			this.eventHandlers = new Dictionary<byte, Action<EventData>>();

			this.enqueuedEvents = new Dictionary<ConnectionStatus, List<EventData>>();

			this.GameState = Disconnected.Instance;
			this.outgoingMessageCount = 0;
		}

		#endregion

		#region Implementation of IConnection

		/// <summary>
		/// Returns the network state
		/// </summary>
		PeerStateValue IConnection.PeerState
		{
			get
			{
				return this.peer.PeerState;
			}
		}

		/// <summary>
		/// Connects to the server
		/// </summary>
		/// <param name="connectionResponseHandler"></param>
		void IConnection.Connect(IConnectResponseHandler connectionResponseHandler)
		{
			this.connectResponseHandler = connectionResponseHandler;
			//peer.Connect(ConnectionSettings.ServerAddress, ConnectionSettings.Application);
			peer.Connect(Configuration.Default.ConnectionString, Configuration.Default.MasterApplication);
			this.GameState = WaitingForConnect.Instance;
		}

		/// <summary>
		/// Updates the connection
		/// </summary>
		void IConnection.Update()
		{
			if(peer.PeerState != PeerStateValue.Disconnected)
				this.peer.Service();
		}

		/// <summary>
		/// Disconnects the connection
		/// </summary>
		void IConnection.Disconnect()
		{
			this.peer.Disconnect();
		}

		#endregion

		#region Implementation of IPhotonPeerListener

		void IPhotonPeerListener.DebugReturn(DebugLevel level, string message)
		{
			Logger.Debug(message);
		}

		void IPhotonPeerListener.OnEvent(EventData eventData)
		{
			Action<EventData> eventHandler;
			if (eventHandlers.TryGetValue(eventData.Code, out eventHandler))
			{
				eventHandler(eventData);
				// only one method can handle a certain event
				// so we would skip the game state handling the event
				return;
			}

			if (!GameState.HandleEvent(eventData))
				Logger.DebugFormat("[OnEvent]: Event (Code={0} ({1})) not handled.", eventData.Code, (ClientEventCode) eventData.Code);
		}

		void IPhotonPeerListener.OnOperationResponse(OperationResponse operationResponse)
		{
			Action<OperationResponse> operationResponseHandler;
			if (operationResponseHandlers.TryGetValue(operationResponse.OperationCode, out operationResponseHandler))
			{
				operationResponseHandler(operationResponse);
				// only one method can handle a operation response
				// so we would skip the game state handling the operation response
				return;
			}

			if (!GameState.HandleOperationResponse(operationResponse))
			{
				Logger.DebugFormat("[OnOperationResponse]: OperationResponse (Code={0} ({1})) not handled.", operationResponse.OperationCode,
								   (ClientOperationCode)operationResponse.OperationCode);
			}
		}

		void IPhotonPeerListener.OnStatusChanged(StatusCode statusCode)
		{
			switch (statusCode)
			{
				case StatusCode.Connect:
					this.peer.EstablishEncryption();
					break;

				case StatusCode.Disconnect:
				case StatusCode.DisconnectByServer:
				case StatusCode.DisconnectByServerLogic:
				case StatusCode.DisconnectByServerUserLimit:
				case StatusCode.TimeoutDisconnect:
					{
						this.connectResponseHandler.OnDisconnected();
						this.GameState = Disconnected.Instance;
					}
					break;

				case StatusCode.EncryptionEstablished:
					{
						// our game is only fully connected when we have established encryption
						this.GameState = Connected.Instance;
						this.connectResponseHandler.OnConnected();
					}
					break;

				default:
					Logger.Debug("Unhandled Status: " + statusCode);
					break;
			}
		}

		#endregion

		#region Implementation of IMessageDispatcher

		/// <summary>
		/// Adds an operation handler to be called when an <see cref="OperationResponse"/> is received. If an existing operation handler
		/// is found it will be invoked and a value of <value>NULL</value> will be passed and removed before adding the new handler.
		/// There can be only one operation handler for a particular <see cref="OperationResponse"/>.
		/// </summary>
		/// <param name="operationCode"></param>
		/// <param name="responseHandler"></param>
		/// <returns></returns>
		IDisposable IMessageDispatcher.RegisterOperationResponseHandler(byte operationCode, Action<OperationResponse> responseHandler)
		{
			Action<OperationResponse> existingHandler;
			if (operationResponseHandlers.TryGetValue(operationCode, out existingHandler))
				throw new InvalidOperationException("HandlerAlreadyRegistered");

			var disposable = new OperationResponseSubscriptionDisposable(operationCode, this);
			operationResponseHandlers.Add(operationCode, responseHandler);
			return disposable;
		}

		/// <summary>
		/// Removes an operation response handler
		/// </summary>
		/// <param name="operationCode"></param>
		void RemoveOperationResponseHandler(byte operationCode)
		{
			operationResponseHandlers.Remove(operationCode);
		}

		/// <summary>
		/// Adds an event handler to be called when an <see cref="EventData"/> is received. If an existing event handler
		/// is found it will be invoked and a value of <value>NULL</value> will be passed and removed before adding the new handler.
		/// There can be only one event handler for a particular <see cref="EventData"/>.
		/// </summary>
		/// <param name="eventCode"> </param>
		/// <param name="eventHandler"></param>
		/// <returns></returns>
		IDisposable IMessageDispatcher.RegisterEventHandler(byte eventCode, Action<EventData> eventHandler)
		{
			Action<EventData> existingHandler;
			if (eventHandlers.TryGetValue(eventCode, out existingHandler))
				throw new InvalidOperationException("HandlerAlreadyRegistered");

			var disposable = new EventSubscriptionDisposable(eventCode, this);
			eventHandlers.Add(eventCode, eventHandler);
			return disposable;
		}

		/// <summary>
		/// Removes an event handler
		/// </summary>
		/// <param name="eventCode"></param>
		void RemoveEventHandler(byte eventCode)
		{
			eventHandlers.Remove(eventCode);
		}

		#endregion

		#region Implementation SubscriptionDisposable class

		class OperationResponseSubscriptionDisposable : IDisposable
		{
			private readonly byte operationCode;
			private readonly NetworkEngine networkEngine;

			public OperationResponseSubscriptionDisposable(byte operationCode, NetworkEngine networkEngine)
			{
				this.operationCode = operationCode;
				this.networkEngine = networkEngine;
			}

			#region Implementation of IDisposable

			/// <summary>
			/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
			/// </summary>
			/// <filterpriority>2</filterpriority>
			public void Dispose()
			{
				networkEngine.RemoveOperationResponseHandler(operationCode);
			}

			#endregion
		}

		class EventSubscriptionDisposable : IDisposable
		{
			private readonly byte eventCode;
			private readonly NetworkEngine networkEngine;

			public EventSubscriptionDisposable(byte eventCode, NetworkEngine networkEngine)
			{
				this.eventCode = eventCode;
				this.networkEngine = networkEngine;
			}

			#region Implementation of IDisposable

			/// <summary>
			/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
			/// </summary>
			/// <filterpriority>2</filterpriority>
			public void Dispose()
			{
				networkEngine.RemoveEventHandler(eventCode);
			}

			#endregion
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Sends an operation to the server
		/// </summary>
		public void SendOperation(ClientOperationCode operationCode, Dictionary<byte, object> parameters)
		{
			this.GameState.SendOperation(peer, operationCode, parameters, false);
			this.OnSend();
		}

		/// <summary>
		/// Sends an operation to the server
		/// </summary>
		public void SendOperation(ClientOperationCode operationCode, Dictionary<byte, object> parameters, bool encrypt)
		{
			this.GameState.SendOperation(peer, operationCode, parameters, encrypt);
			this.OnSend();
		}

		/// <summary>
		/// Enqueues a message to be handled at a certain game state
		/// </summary>
		/// <param name="eventData"></param>
		/// <param name="connectionStatus"></param>
		public void EnqueueMessageHandling(EventData eventData, ConnectionStatus connectionStatus)
		{
			if(connectionState.Status == connectionStatus)
			{
				this.connectionState.HandleEvent(eventData);
			}
			else
			{
				List<EventData> eventList;
				if(!enqueuedEvents.TryGetValue(connectionStatus, out eventList))
				{
					eventList = new List<EventData>();
					enqueuedEvents.Add(connectionStatus, eventList);
				}
				eventList.Add(eventData);
			}
		}
		
		protected void OnSend()
		{
			outgoingMessageCount++;

			if (outgoingMessageCount > 10)
			{
				this.peer.SendOutgoingCommands();
				outgoingMessageCount = 0;
			}
		}

		#endregion
	};
}