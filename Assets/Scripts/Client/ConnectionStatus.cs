﻿namespace Karen90MmoFramework.Client
{
	public enum ConnectionStatus : byte
	{
		Disconnected,
		WaitingForConnect,
		Connected,
		WorldEntered
	}
}
