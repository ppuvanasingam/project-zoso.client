using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client
{
	public class MinimapSystem : MonoBehaviour, IMinimap
	{
		private const int MaxZoomValue = 100;
		private const int MinZoomValue = 45;
		private const int ZoomStepValue = 15;

		private const float ZoomPercentFactor = 1f / (MaxZoomValue - MinZoomValue);

		/// <summary>
		/// Gets the zoom percent
		/// </summary>
		float IMinimap.ZoomPercent
		{
			get
			{
				return (GetComponent<Camera>().orthographicSize - MinZoomValue) * ZoomPercentFactor;
			}
		}

		private MapHandler mapHandler;
		private Player player;

		private const float MapCheck = 5;
		private float timer;

		private readonly Dictionary<IBlip, GameObject> pois = new Dictionary<IBlip, GameObject>();

		void Awake()
		{
			var bundle = AssetBundle.LoadFromFile(string.Format("{0}/{1}", System.IO.Directory.GetCurrentDirectory(), "Data/map.dat"));
			
			var settingsData = bundle.LoadAsset<TextAsset>("settings");
			if (settingsData != null)
			{
				var mapSettings = new MapSettings(settingsData.text);
				this.mapHandler = new MapHandler(this, bundle, mapSettings, LayerMask.NameToLayer("Map"));
			}

			this.GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize, MinZoomValue, MaxZoomValue);
		}

		/// <summary>
		/// Sets up the <see cref="MinimapSystem"/>.
		/// </summary>
		/// <param name="player"></param>
		public void Setup(Player player)
		{
			this.player = player;

			this.MoveCam(player.Position);
			this.mapHandler.Start(player.Position);
		}

		public void Unload()
		{
			this.mapHandler.Unload();
			this.mapHandler = null;
		}

		/// <summary>
		/// Adds a poi
		/// </summary>
		/// <param name="poi"></param>
		void IMinimap.AddPOI(IBlip poi)
		{
			((IMinimap)this).RemovePOI(poi);

			var go = ((GameObject)Instantiate(GameResources.Instance.GoBlipHolder, new Vector3(poi.Position.x, 1, poi.Position.z), Quaternion.Euler(90, 0, 0)));
			var scaleVal = (GetComponent<Camera>().orthographicSize / 60);
			var scale = new Vector3(scaleVal, scaleVal, scaleVal);

			go.transform.localScale = scale;
			go.GetComponent<Renderer>().material.mainTexture = poi.BlipIcon;

			this.pois.Add(poi, go);
		}

		/// <summary>
		/// Removes a poi
		/// </summary>
		/// <param name="poi"></param>
		void IMinimap.RemovePOI(IBlip poi)
		{
			GameObject oldGameobject;
			if (pois.TryGetValue(poi, out oldGameobject))
			{
				Destroy(oldGameobject);
				this.pois.Remove(poi);
			}
		}

		/// <summary>
		/// Updates the poi
		/// </summary>
		/// <param name="poi"></param>
		void IMinimap.UpdatePOI(IBlip poi)
		{
			GameObject poiObject;
			if (pois.TryGetValue(poi, out poiObject))
			{
				poiObject.GetComponent<Renderer>().material.mainTexture = poi.BlipIcon;
				poiObject.transform.position = new Vector3(poi.Position.x, 1, poi.Position.z);
			}
		}

		/// <summary>
		/// Zooms in the map
		/// </summary>
		void IMinimap.ZoomIn()
		{
			var oldSize = GetComponent<Camera>().orthographicSize;
			var newSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize - ZoomStepValue, MinZoomValue, MaxZoomValue);
			if (Mathf.Abs(oldSize - newSize) >= ZoomStepValue)
			{
				this.GetComponent<Camera>().orthographicSize = newSize;
				this.UpdatePOIScales();
			}
		}

		/// <summary>
		/// Zooms out the map
		/// </summary>
		void IMinimap.ZoomOut()
		{
			var oldSize = GetComponent<Camera>().orthographicSize;
			var newSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize + ZoomStepValue, MinZoomValue, MaxZoomValue);
			if (Mathf.Abs(oldSize - newSize) >= ZoomStepValue)
			{
				this.GetComponent<Camera>().orthographicSize = newSize;
				this.UpdatePOIScales();
			}
		}

		void UpdatePOIScales()
		{
			var scaleVal = (GetComponent<Camera>().orthographicSize / 60);
			var scale = new Vector3(scaleVal, scaleVal, scaleVal);
			foreach (var poi in pois)
			{
				poi.Value.transform.localScale = scale;
			}
		}

		void Update()
		{
			this.MoveCam(player.Position);

			timer += Time.deltaTime;
			if (timer > MapCheck)
			{
				this.mapHandler.UpdateMap(player.Position);
				this.timer = 0;
			}
		}

		void MoveCam(Vector3 pos)
		{
			this.transform.position = new Vector3(pos.x, this.transform.position.y, pos.z);
		}

		public void StartAsyncMethod(IEnumerator method)
		{
			this.StartCoroutine(method);
		}
	};
}
